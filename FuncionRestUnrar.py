import json
from flask import Flask
import sys
from rarfile import RarFile
import os
import boto3
import rarfile
app = Flask(__name__)

access_key = 'AKIARRWTZ3AZLKQBPBRS'
secret_key = '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'
region ='us-east-2'
rar_decompress ='FILE_NAME.rar'




#Metodo Rest expuesto para descomprimir un archivo rar
#parametro nombreBucket : nombre del bucket donde esta el rar 
#parametro nombreArchivo : nombre del archivo rar 

@app.route('/<nombreBucket>/<nombreArchivo>')
def descomprimirRar(nombreBucket: str, nombreArchivo: str):
    try:
        s3 = conectarAWS()
        s3.download_file(nombreBucket, nombreArchivo,rar_decompress)

        rutaExtraccionArchivos=nombreArchivo.replace(".rar","")+'/'
        descomprimirArchivo(rutaExtraccionArchivos)

        for path, subdirs, archivos in os.walk(rutaExtraccionArchivos):
            for archivo in archivos:
                response = s3.upload_file(rutaExtraccionArchivos+archivo, nombreBucket, rutaExtraccionArchivos+archivo)

        eliminarArchivosTemporales(rutaExtraccionArchivos)
        
        return {
            'statusCode': 200,
            'mensaje': ('Rar extraido con exito'),
            'nombreBucket': nombreBucket,
            'carpetaBucket': rutaExtraccionArchivos
        }

    except BaseException as e:    
        return {
            'statusCode': 409,
            'mensaje': ('Fallo: '+str(e))
        }


#Metodo que crea la conexion a cuenta de aws
def conectarAWS():
 try:
    return boto3.client('s3',
               aws_access_key_id=access_key,
               aws_secret_access_key=secret_key,
               region_name= region)
 except Exception as e:    
    raise Exception('No se pudo conectar a la cuenta de AWS: '+str(e))


#Metodo que busca el rar descargado y lo descomprime en una ruta especifica
def descomprimirArchivo(rutaCarpeta:str):
 try:
   
    archivosExtraidos = []
    with RarFile(rar_decompress) as archivoRar:
      contadorArchivos=0  
      for f in archivoRar.namelist():
       archivoRar.extract(archivoRar.namelist()[contadorArchivos],rutaCarpeta)
       archivosExtraidos.append(open(rutaCarpeta+archivoRar.namelist()[contadorArchivos], "rb").read())
       contadorArchivos+=1

 except BaseException as e:    
     raise BaseException('No se pudo descomprimir el archivo: '+str(e))


#Metodo que elimina los archivos descargados y decompresos temporalmente
def eliminarArchivosTemporales(rutaCarpeta:str):
    try:
        os.remove(rar_decompress)
        for path, subdirs, archivos in os.walk(rutaCarpeta):
            for archivo in archivos:
                os.remove(rutaCarpeta+archivo)
        os.rmdir(rutaCarpeta)

    except Exception as e:    
        raise BaseException ('Error al eliminar los archivos Temporales: '+str(e) )


if __name__ == '__main__':
    app.run(debug=True, port=8080, host = '0.0.0.0')

