FROM alpine
RUN apk upgrade --no-cache \
  && apk add --no-cache \
    musl \
    build-base \
    python3 \
    python3-dev \
    postgresql-dev \
    bash \
    git \
    tar \
    tiff \
    unrar \
    unzip \
    vnstat \
    wget \
    xz \
    curl \
    freetype \
    lcms2 \
    libjpeg-turbo \
    libwebp \
    openjpeg \
    openssl \
    p7zip \
  && pip3 install --no-cache-dir --upgrade pip \
  && rm -rf /var/cache/* \
  && rm -rf /root/.cache/*

RUN cd /usr/bin \
  # && ln -sf easy_install-3.5 easy_install \
  && ln -sf python3 python \
  && ln -sf pip3 pip 

RUN pip install boto  --no-cache  \
 && pip install boto3  --no-cache  \
 && pip install rarfile  --no-cache \
 && pip install flask   --no-cache 

RUN git clone https://gitlab.com/jonathanpena/python-unrar.git

CMD cd python-unrar/  \
&& python FuncionRestUnrar.py


EXPOSE 8080/tcp